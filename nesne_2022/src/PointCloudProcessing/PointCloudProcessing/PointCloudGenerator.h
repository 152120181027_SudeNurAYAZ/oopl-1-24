/**
* @file       PointCloudGenerator.h
* @author     Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date       29 Aral�k 2021 Carsamba
*/
#pragma once
#include"Transform.h"
#include"PointCloud.h"
#include"FilterPipe.h"
class PointCloudGenerator 
{
	Transform transform;
	FilterPipe* fPipe;

public:
	
	PointCloudGenerator();
	virtual PointCloud capture() = 0;
	virtual PointCloud captureFor() = 0;
	void setfilterpipe(FilterPipe*);
	~PointCloudGenerator();
};

