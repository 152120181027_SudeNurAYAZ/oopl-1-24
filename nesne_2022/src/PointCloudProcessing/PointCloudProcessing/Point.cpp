﻿/**
* @file Point.cpp
* @Author Betul AKSU (aksubb000@gmail.com)
* @date December, 2021
* @brief This class holds the coordinates of 3D points in the point cloud.
*/

#include<iostream>
#include<math.h>
#include "Point.h"

using namespace std;

/**
	* @brief constructor function
	*/
Point::Point() {

}

/**
	* @brief destructor function
	*/
Point::~Point() {

}

/**
	* @brief  default constructor
	* @param  _x a double argument.
	* @param  _y a double argument.
	* @param  _z a double argument.
	*/
Point::Point(double _x = 0, double _y = 0, double _z = 0)
	:x(_x), y(_y), z(_z) {

}

/**
	* @brief  this function assign values ​​to x, y and z
	* @param  _x a double argument.
	* @param  _y a double argument.
	* @param  _z a double argument.
	*/
void Point::setXYZ(double _x, double _y, double _z)
{
	x = _x;
	y = _y;
	z = _z;
}

/**
	* @brief this function access the x value
	*/
double Point::getX() const {
	return x;

}

/**
	* @brief this function access the y value
	*/
double Point::getY() const {
	return y;
}

/**
	* @brief this function access the z value
	*/
double Point::getZ() const {
	return z;
}

/**
	* @brief this function checks if two points are equal
	* @param P a Point argument.
	*/
bool Point::operator==(const Point P)const
{
	return (this->x == P.x && this->y == P.y && this->z == P.z);

}

/**
	* @brief this function finds the distance between two points
	* @param P a Point argument.
	*/
double Point::distance(const Point& P) const {

	return sqrt((this->x - P.x) * (this->x - P.x) + (this->y - P.y) * (this->y - P.y) + (this->z - P.z) * (this->z - P.z));
}

