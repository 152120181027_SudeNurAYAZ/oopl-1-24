/**
* @file       PassThroughFilter.cpp
* @author     Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date       29 Aral�k 2021 Carsamba
* @brief      headerda olu�turulan fonksiyonlari isleyisini icerir.
*/
#include "PassThroughFilter.h"
#include<list>
using namespace std;
/**
	@brief:			Bu fonksiyon yapici fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PassThroughFilter::PassThroughFilter()
{

}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PassThroughFilter::~PassThroughFilter()
{

}
/**
	@brief:			Bu fonksiyon x icin deger atamalarini yapan fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void PassThroughFilter::setUX(double _upperLimitX)
{
	this->uLX = _upperLimitX;
}
void PassThroughFilter::setLX(double _lowerLimitX)
{
	this->lLX = _lowerLimitX;
}
/**
	@brief:			Bu fonksiyon y icin deger atamalarini yapan fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void PassThroughFilter::setUY(double _upperLimitY)
{
	this->uLY = _upperLimitY;
}
void PassThroughFilter::setLY(double _lowerLimitY)
{
	this->lLY = _lowerLimitY;
}
/**
	@brief:			Bu fonksiyon z icin deger atamalarini yapan fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void PassThroughFilter::setUZ(double _upperLimitZ)
{
	this->uLZ = _upperLimitZ;
}
void PassThroughFilter::setLZ(double _lowerLimitZ)
{
	this->lLZ = _lowerLimitZ;
}
/**
	@brief:			Bu fonksiyon xin max degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double PassThroughFilter::getUX()
{
	return this->uLX;
}
/**
	@brief:			Bu fonksiyon xin min degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double PassThroughFilter::getLX()
{
	return this->lLX;
}
/**
	@brief:			Bu fonksiyon ynin max degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double PassThroughFilter::getUY()
{
	return this->uLY;
}
/**
	@brief:			Bu fonksiyon ynin min degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double PassThroughFilter::getLY()
{
	return this->lLY;
}
/**
	@brief:			Bu fonksiyon znin max degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double PassThroughFilter::getUZ()
{
	return this->uLZ;
}
/**
	@brief:			Bu fonksiyon znin min degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double PassThroughFilter::getLZ()
{
	return this->lLZ;

}
/**
	@brief:			Bu fonksiyon nokta bulutunu filtrelemeye yarayan fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void PassThroughFilter::Filters(PointCloud& obj)
{
	list<Point> string1, string2;
	list<Point>::iterator it;
	string1 = obj.getPoints();
	PointCloud obj2;

	for (it = string1.begin(); it != string1.end(); it++)
	{

		if (((*it).getX() <= getUX() && (*it).getX() >= getLX()) &&
			(((*it).getY() <= getUY() && (*it).getY() >= getLY())) &&
			((*it).getZ() <= getUZ() && (*it).getZ() >= getLZ()))
		{
			string2.push_back(*it);//Filitrelenen noktalar str2 listesinde tutulmustur.
		}
	}
	obj.setPoints(string2);//Filitrelenen noktalari iceren nokta bulutu dondurulmustur.
}