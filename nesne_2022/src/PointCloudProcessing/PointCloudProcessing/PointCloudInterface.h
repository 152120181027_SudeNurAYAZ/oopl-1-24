/**
* @file		PointCloudInterface.h
* @Author	�zge K�branur OMAR	 e-mail=(kubraomar.8@gmail.com)
* @date		January 2022
* @brief	Bu s�n�f i�lemleri basitle�tirmek i�in kullan�lan bir s�n�ft�r.
*/
#include"PointCloud.h"
#include"PointCloudGenerator.h"
#include"PointCloudRecorder.h"
#include<vector>
#pragma once

class PointCloudInterface
{
	PointCloud Pointcloud;
	PointCloud Patch;
	vector<PointCloudGenerator*> Generators;
	PointCloudRecorder* Recorder;
public:
	PointCloudInterface();
	~PointCloudInterface();
	void Addgenerator(PointCloudGenerator*);
	void Setrecorder(PointCloudRecorder*);
	bool Generate();
	bool Record();
};

