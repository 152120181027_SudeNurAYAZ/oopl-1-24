/**
* @file TestTransform.cpp
* @Author Muhammet Emin Kurt (mekurt55@gmail.com)
* @date December, 2021
* @brief: Tests the Transform class
*/
#include "Transform.h"
#include "PointCloudRecorder.h"
#include "PointCloud.h"
#include "Point.h"
#include<vector>
#include<iostream>
#include<vector>

using namespace std;

int main(void)
{
	double testAngle[3] = { 0,0,90 }, testTrans[3] = { 0,0,90 }, testTransMatrix[4][4] = {0};

	Transform test1, test2, test3;
	Transform test4, test5, test6;
	Point point;
	vector<Point> vectorPoint;
	PointCloud pointCloud;

	test1.setAngles(testAngle);
	test2.setTrans(testTrans);
	test3.setTransMatrix(testTransMatrix);

	test1.getAngles();
	test2.getTrans();

	test1.setRotation(testAngle);
	test2.setTranslation(testTrans);
	test3.doTransform(point);
	test4.doTransform(vectorPoint);
	test5.doTransform(pointCloud);

	return 0;
}