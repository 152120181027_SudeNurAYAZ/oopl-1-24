
/**
* @file PointCloud.h
* @Author Betul AKSU (aksubb000@gmail.com)
* @date December, 2021
* @brief It keeps its points in a dynamically created Point array.
*/
#pragma once
#include"Point.h"
#include<list>
#include<vector>
#include<iterator>
class PointCloud
{
private:
	list<Point> points;
	list<Point>::iterator pointsIter;
	int pointNumber;
public:
	PointCloud();
	~PointCloud();
	PointCloud(list<Point>);
	int pointsSize();
	void setPointNumber(int);
	int getPointNumber() const;
	void setPoints(list<Point>);
	list<Point> getPoints() const ;
	PointCloud operator+(PointCloud&);
	void operator=(PointCloud*);

};

