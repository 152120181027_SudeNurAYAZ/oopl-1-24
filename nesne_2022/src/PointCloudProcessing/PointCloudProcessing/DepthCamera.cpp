/**
* @file		DepthCamera.cpp
* @Author   Sude Nur Ayaz (ssudeayazz@gmail.com)
* @date     January, 2022
*/
#include "DepthCamera.h"
/**
	@brief:			Bu fonksiyon yapici fonksiyondur.Filename olusturur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
DepthCamera::DepthCamera() :fileName(fileName)
{
	string fileName = "";
}
/**
	@brief:			Bu fonksiyon filename duzenlemeyi saglayan fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void DepthCamera::setDept(string fileName_)
{
	this->fileName = fileName_;
}
/**
	@brief:			Bu fonksiyon filename private olarak kullanmayi saglayan fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
string DepthCamera::getDept()
{
	return this->fileName;
}
/**
	@brief:			Bu fonksiyon acilan dosyanin icindeki degerleri okuyan ve pointlerin icine atamalari yapan fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloud DepthCamera::capture()
{
	fstream dosya;
	list<Point> H;
	PointCloud pc;
	Point i;
	string dsy = this->getDept();

	dosya.open(dsy, ios::in);

	if (dosya.fail())
	{
		cout << "the file could not opened";
		exit(1);
	}
	else {
		double x, y, z;
		while (!dosya.eof()) {

			dosya >> x;
			dosya >> y;
			dosya >> z;
			i.setXYZ(x, y, z);

			H.push_back(i);
		}
		dosya.close();
		pc.setPoints(H);

	}
	return pc;
}
/*!
Noktalari okur.Filtreleme ve transform islemlerini yapar.
*/
/**
	@brief:			Bu fonksiyon dosyadan cekilen noktalari okur.Filtreleme ve transform islemlerini yapar.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloud DepthCamera::captureFor()
{
	Transform TRNS;
	PointCloud pc1;
	string a;
	pc1=this->capture();
	a=this->getDept();
	FilterPipe p1;
	PointCloud Total, YPC1, YPC2;
	RadiusOutlierFilter rfilter;
	PassThroughFilter pfilter;
	rfilter.Set(25);
	p1.addFilter(&rfilter);
	p1.addFilter(&pfilter);

	if (a == "camera1.txt")
	{	
		pfilter.setLX(0);
		pfilter.setUX(400);
		pfilter.setLY(0);
		pfilter.setUY(400);
		pfilter.setLZ(-45);
		pfilter.setUZ(45);
		p1.filterOut(pc1);

		Eigen::Vector3d a(0.0, 0.0, (-90.0));
		TRNS.Setangles(a);
		Eigen::Vector3d t(100.0, 500.0, 50.0);

		TRNS.Settrans(t);
		
		TRNS.Setrotation(TRNS.Getangles());

		TRNS.Settranslation(TRNS.Gettrans());
		
	}
	if (a == "camera2.txt")
	{
		cout << endl << endl << endl;
		pfilter.setLX(0);
		pfilter.setUX(500);
		pfilter.setLY(0);
		pfilter.setUY(500);
		pfilter.setLZ(-45);
		pfilter.setUZ(45);
		p1.filterOut(pc1);

		p1.filterOut(pc1);
		Eigen::Vector3d a1(0.0, 0.0, (90.0));
		TRNS.Setangles(a1);
		Eigen::Vector3d t1(550.0, 150.0, 50.0);
		TRNS.Settrans(t1);

		TRNS.Setrotation(TRNS.Getangles());

		TRNS.Settranslation(TRNS.Gettrans());
	}
	pc1 = TRNS.Dotransform(pc1);


	return pc1;
}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
DepthCamera::~DepthCamera()
{
}