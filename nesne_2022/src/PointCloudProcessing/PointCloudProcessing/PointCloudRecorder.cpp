/**
 * @file   PointCloudRecorder.cpp
 * @Author Muhammet Emin Kurt email=mekurt55@gmail.com
 * @date   December 2021
 *
*/
#include "PointCloudRecorder.h"
#include"PointCloud.h"
#include"Point.h"
#include<fstream>
#include<iostream>
using namespace std;
/**
	@brief:			Bu fonksiyon yapici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloudRecorder::PointCloudRecorder()
{

}
/**
	@brief:			Bu fonksiyon icine deger yazilacak olan dosyanin ismini d�zenleyen fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void PointCloudRecorder::Setfilename(string newfilename)
{
	this->filename = newfilename;
}
/**
	@brief:			Bu fonksiyon private deger olan filenami donduren fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
string PointCloudRecorder::Getfilename()
{
	return this->filename;
}



/**
	@brief:			Bu fonksiyon ile fileName al�n�r ve dosya acilir.Acilan dosya icine islem gormus ve filtrelenmis numaralar yazilir.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
bool PointCloudRecorder::Save(PointCloud& newpc)
{
	ofstream dosya;
	list<Point> point;
	list<Point>::iterator iter;
	point = newpc.getPoints();
	dosya.open(this->Getfilename(), ios::out);
	for (iter = point.begin(); iter != point.end(); iter++)
		dosya << (*iter).getX() << " " << (*iter).getY() << " " << (*iter).getZ() << endl;
	dosya.close();
	return true;
}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloudRecorder::~PointCloudRecorder()
{

}