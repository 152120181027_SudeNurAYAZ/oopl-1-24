/**
* @file PointCloud.cpp
* @Author Betul AKSU (aksubb000@gmail.com)
* @date December, 2021
* @brief Headerda olu�turulan fonksiyonlari isleyisini icerir..
*/
#include "PointCloud.h"
/**
	@brief:			Bu fonksiyon yapici fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
PointCloud::PointCloud()
{
}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
PointCloud::~PointCloud()
{
}

/**
	@brief:			Bu fonksiyon listeyi olusturur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/

int PointCloud::pointsSize()
{
	return points.size();
}

void PointCloud::setPoints(list<Point>p)
{
	this->points = p;
}
/**
	@brief:			Bu fonksiyon listeye ulasmamizi saglar.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
list<Point> PointCloud::getPoints () const
{
	return points;
}
/**
	@brief:			Bu fonksiyon operator overloading fonksiyonudur ve iki nesneyi toplama islemi yapar.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
PointCloud PointCloud::operator+(PointCloud& A)
{
	list<Point> K1, K2, tmp;
	list<Point>::iterator iter1, iter2;
	PointCloud C;
	K1 = this->getPoints();
	K2 = A.getPoints();
	int j = 0;
	iter1 = K1.begin();
	iter2 = K2.begin();
	int a = K1.size();
	for (int i = 0; i < K1.size() + K2.size(); i++) {
		if (i < K1.size())
		{
			tmp.push_back(*iter1++);
		}
		else
		{
			tmp.push_back(*iter2++);
		}
	}
	
	C.setPoints(tmp);
	return C;
}
/**
	@brief:			Bu fonksiyon operator overloading fonksiyonudur ve bir nesneyi ba�ka bir nesneye kopyalar.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
*/
void PointCloud::operator=(PointCloud *obj)
{
	
	for (pointsIter = obj->points.begin(); pointsIter != obj->points.end(); pointsIter++)
	{
		this->points.push_back(*pointsIter);
    }
}