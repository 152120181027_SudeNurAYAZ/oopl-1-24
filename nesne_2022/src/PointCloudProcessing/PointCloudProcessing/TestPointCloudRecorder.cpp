/**
* @file TestPointCloudRecorder.cpp
* @Author Muhammet Emin Kurt (mekurt55@gmail.com)
* @date December, 2021
* @brief: Tests the PointCloudRecorder class
*/
#include "PointCloudRecorder.h"
#include "DepthCamera.h"
#include "PointCloud.h"
#include "Point.h"
#include<fstream>
#include<vector>
#include<iostream>
using namespace std;

int main(void)
{


	PointCloudRecorder pointCloudRecorder;
	PointCloud pointCloud;
	DepthCamera depthCamera;

	
	depthCamera.setfileName("camera.txt");

	pointCloudRecorder.setFileName("saver.txt");

	pointCloud = depthCamera.capture();
	pointCloudRecorder.save(pointCloud);


	return 0;
}