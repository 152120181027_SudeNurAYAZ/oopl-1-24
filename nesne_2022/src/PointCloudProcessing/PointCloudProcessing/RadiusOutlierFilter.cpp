/**
	@File:			RadiusOutlierFilter.cpp
	@Author:		�zge K�branur OMAR	 e-mail=(kubraomar.8@gmail.com)
	@Date:			December 2021
*/
#include "RadiusOutlierFilter.h"
/**
	@brief:			Bu fonksiyon yapici fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
RadiusOutlierFilter::RadiusOutlierFilter()
{

}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
RadiusOutlierFilter::~RadiusOutlierFilter()
{

}
/**
	@brief:			Bu fonksiyon radius deger atamasini yapan fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void RadiusOutlierFilter::Set(double newradius)
{
	radius = newradius;
}
/**
	@brief:			Bu fonksiyon radius degerini donduren fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
double RadiusOutlierFilter::Get()
{
	return this->radius;
}
/**
	@brief:			Bu fonksiyon nokta bulutunu filtrelemeye yarayan fonksiyondur..
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void RadiusOutlierFilter::Filters(PointCloud& obj)
{
	list<Point> p1, p2;
	list<Point>::iterator iter1, iter2;
	Point st;
	p1 = obj.getPoints();
	int i = 0, j = 0;
	for (iter1 = p1.begin(); iter1 != p1.end(); iter1++)
	{
		for (iter2 = p1.begin(); iter2 != p1.end(); iter2++)
		{
			if ((*iter1).distance(*iter2) < Get() && iter1 != iter2)
			{
				p2.push_back(*iter1);
				break;
			}
		}
	}
	obj.setPoints(p2);

}
