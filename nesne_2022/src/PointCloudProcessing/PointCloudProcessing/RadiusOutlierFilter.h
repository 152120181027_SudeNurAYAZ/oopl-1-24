/**
	@File:			RadiusOutlierFilter.h
	@Author:		�zge K�branur OMAR	 e-mail=(kubraomar.8@gmail.com)
	@Date:			December 2021
	@Brief:			Bu kod parcacigi derinlik kamerasi fonksiyonlarini calistirir.
*/

#pragma once
#include "PointCloud.h"
#include"PointCloudFilter.h"

class RadiusOutlierFilter :public PointCloudFilter
{
	double radius;
public:
	RadiusOutlierFilter();
	void Set(double);
	double Get();
	void Filters(PointCloud&);
	~RadiusOutlierFilter();
};

