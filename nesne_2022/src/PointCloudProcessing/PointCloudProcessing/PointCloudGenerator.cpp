/**
* @file       PointCloudGenerator.cpp
* @author     Yasin Binler  e-mail: yasinbinler06@gmail.com
* @date       29 Aral�k 2021 Carsamba
* @brief      Headerda olu�turulan fonksiyonlari isleyisini icerir.
*/
#include "PointCloudGenerator.h"
/**
	@brief:			Bu fonksiyon yapici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloudGenerator::PointCloudGenerator()
{

}
/**
	@brief:			Bu fonksiyon filterPipe nesnesinin degerinin belirlenmesini saglar.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
void PointCloudGenerator::setfilterpipe(FilterPipe* fp)
{
	this->fPipe = fp;
}
/**
	@brief:			Bu fonksiyon yikici fonksiyondur.
	@see main():	Fonksiyonun hangi amacla cagirildigini inceleyiniz.
	*/
PointCloudGenerator::~PointCloudGenerator()
{

}