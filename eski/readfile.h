/* readfile.h */
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include <string>
using namespace std;

class ReadFile {
    private:
    	string filename;

    public:
    	char buffer[100];
    	void open_file(int,char* []);
   		void close_file();

};

