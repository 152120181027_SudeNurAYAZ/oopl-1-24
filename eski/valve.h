
#include<iostream>
#include"tank.h"
#include"engine.h"
using namespace std;

class Valve
{
public:
    void open_valve(int);
    void close_valve(int);
    void start_valve();
	void stop_valve();

private:
    int _tank_id;
    engine Engine;
    tank Tank;
    bool valveStatus;
};
