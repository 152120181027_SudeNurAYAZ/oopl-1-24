/* readfile.cpp */
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include "readfile.h"
#include <string>


	void ReadFile::open_file(int argc, char* argv[]) {
    	
    	fstream inputfile;
    	for(int i=1; i <= argc; i++){
    		
			cout << "Enter the file's name which you want to open :";
			this->filename=argv[i];
			inputfile.open(this->filename, ios::in);
			if (!inputfile)
			{
				cout << "Error! File can not be opened!";
				exit(0);
			}
		}	
	}
		
		while(1){
			
			inputfile.getline(buffer, 100, ' ');
		
			if(buffer=="start_engine;"){
			
			}
			else if(buffer=="stop_engine;"){
				
			
			}
			else if(buffer=="absorb_fuel"){
				inputpile.getline(buffer,100,';');
				int absorb_fuel_quantity;
				absorb_fuel_quantity=atoi(buffer);
			
			}
			else if(buffer=="give_back_fuel"){
				inputpile.getline(buffer,100,';');
				int give_back_fuel_quantity;
				give_back_fuel_quantity=atoi(buffer);
			
			}
			else if(buffer=="add_fuel_tank"){
				inputpile.getline(buffer,100,';');
				int add_fuel_tank_capacity;
				add_fuel_tank_capacity=atoi(buffer);
			
			}
			else if(buffer=="list_fuel_tanks;"){
			
			
			}
			else if(buffer=="print_fuel_tank_count;"){
			
			
			}
			else if(buffer=="remove_fuel_tank"){
				inputpile.getline(buffer,100,';');
				int remove_fuel_tank_id;
				remove_fuel_tank_id=atoi(buffer);
			
			}
			else if(buffer=="connect_fuel_tank_to_engine"){
				inputpile.getline(buffer,100,';');
				int connect_fuel_tank_to_engine_id;
				connect_fuel_tank_to_engine_id=atoi(buffer);
				
			}
			else if(buffer=="disconnect_fuel_tank_from_engine"){
				inputpile.getline(buffer,100,';');
				int disconnect_fuel_tank_from_engine_id;
				disconnect_fuel_tank_from_engine_id=atoi(buffer);
			
			}else if(buffer=="list_connected_tanks;"){
			
			
			}else if(buffer=="print_total_fuel_quantity;"){
			
			
			}else if(buffer=="print_total_consumed_fuel_quantity;"){
			
			
			}else if(buffer=="print_tank_info"){
				inputpile.getline(buffer,100,';');
				int print_tank_info_id;
				print_tank_info_id=atoi(buffer);
			
			}else if(buffer=="fill_tank"){
				inputpile.getline(buffer,100,' ');
				int fill_tank_id;
				fill_tank_id=atoi(buffer);
				inputpile.getline(buffer,100,';');
				int fill_tank_quantity;
				fill_tank_quantity=atoi(buffer);
				
			}else if(buffer=="open_valve"){
				inputpile.getline(buffer,100,';');
				int open_valve_id;
				open_valve_id=atoi(buffer);
			
			}else if(buffer=="close_valve"){
				inputpile.getline(buffer,100,';');
				int close_valve_id;
				close_valve_id=atoi(buffer);
			
			}else if(buffer=="break_fuel_tank"){
				inputpile.getline(buffer,100,';');
				int break_fuel_tank_id;
				break_fuel_tank_id=atoi(buffer);
			
			}else if(buffer=="repair_fuel_tank"){
				inputpile.getline(buffer,100,';');
				int repair_fuel_tank_id;
				repair_fuel_tank_id=atoi(buffer);
			
			}else if(buffer=="wait"){
				inputpile.getline(buffer,100,';');
				int wait_seconds;
				wait_seconds=atoi(buffer);
			
			}
			else if(buffer=="stop_simulation;"){
			
				break;
			}
		
		}
		
    
	
	

	void ReadFile::close_file(){
    	inputfile.close();
	}

