/* tank.h*/

#ifndef TANK_H_INCLUDED
#define TANK_H_INCLUDED

#include<iostream>
using namespace std;


class Tank{
	private:
		const double capacity=55;
		double fuel_quuantity;
		bool broken;
	
	public:
		Tank();
		void add_fuel_tank(double);
		void list_fuel_tank();
		void remove_fuel_tank(int);
		void connect_fuel_tank_to_engine(int);
		void disconnect_fuel_tank_from_engine(int);
		void break_fuel_tank(int);
		void repair_fuel_tsank(int); 
		void print_tank_info(int);
		void print_total_fuel_quantity();
		void print_total_consumed_fuel_quantity();
		void fill_tank(int,int);
		void print_fuel_tank_count();
		void start_tank();
		void stop_tank();

};
#endif
