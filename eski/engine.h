/*engine.h*/
#include<iostream>
#include<vector>
#include"tank.h"
#include"tank.cpp"
using namespace std;

class Engine{
	private:
		const double fuel_per_second=5.5;
		bool status; /* if this true=engine running*/
		vector<tank> Tank;
	public:
		engine();
		void start_engine();
		void stop_engine();
		void absorb_fuel(double);
		void give_back_fuel(double);
		void stop_simulation();
		bool get_status();
};
